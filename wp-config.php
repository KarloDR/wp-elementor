<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-elementor' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3307' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.gNJ;m9,DJ-wJQ#f+6;9yNr~le7`yu8TdU-x/?kKyef5~tLV;rbEM=)0vEm^z3lN' );
define( 'SECURE_AUTH_KEY',  ';`tc1qLKC{~;J*A=!4|oV>T9PUIQ?x@RQpGH;WfW`gV%e4>Ukvwh-lBuuR{Pk$,o' );
define( 'LOGGED_IN_KEY',    'o:d+R&*G?O~mP2KJr o2wy~u/2w|@y.aN5gA8c~P}wEwD*dy;s3^S95;xI) /CQI' );
define( 'NONCE_KEY',        'l(y*kf[0]nx8AqF?;Sa;cV6NxpbEKJng9mfjWtw9-gz du>C$,tk8KQhZS!7B2W=' );
define( 'AUTH_SALT',        ' (Ez%N#_Y-0dNH`?am>b6cRcqL~EdcFaCD4$:5]FTF7_e#c:_:=+?C]s|Z1gg1jG' );
define( 'SECURE_AUTH_SALT', '56|2k#({g=:`Y,8aoyq<>V0q#OTMMnEVQZsAXk<A=0y9^m]A1sQ8Y`a*d/GsT{0e' );
define( 'LOGGED_IN_SALT',   '-0|hRv9!%`/sv@jXUjm4T}D3XP0p+_-Il}<:ScsVA@0`51@+Heqzeyth*G|?NP:r' );
define( 'NONCE_SALT',       'ZF{0Wf~HR@u;`vcMD4rcu7PZ+/QP[siCxODHSlZWaD5>,titom-n2MUy)|85zhyJ' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
